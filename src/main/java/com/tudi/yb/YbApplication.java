package com.tudi.yb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YbApplication {

  public static void main(String[] args) {
    SpringApplication.run(YbApplication.class, args);
  }

}
