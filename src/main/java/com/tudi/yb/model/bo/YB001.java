package com.tudi.yb.model.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class YB001 {

  private String rdzlsh;
  private String czyzh;
  private String dzpz;
}
